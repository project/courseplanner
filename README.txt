This project provides tools for managing course planning, as well as a library
for teaching/learning resources.

## What you can do with Course planner

* Create 'course offerings', being actual offerings of a course. These have
  lessons, a calendar (which is exportable), printable week lists, and more.
* Create 'course sections', being rich descriptions of lessons. Sections may be
  used across multiple lessons.
* Create 'resources' that you (or your students) want to use in your lessons.
  Resources can be book references, links, file uploads, and what not. Resources
  are usually attached to sections, but can be stand-alone content as well.
* Create 'course outlines', being a set of sections you think make a good way to
  teach a course. These can be re-used across multiple course offerings, to
  easily populate the lessons in an offering.

## Some of the extra features

* The resources are collected in a library, and can be re-used by others. The
  library has faceted search, to make it easier to find relevant resources.
* The lessons for a course offering are provided as an online calendar and as a
  printable per-week list. They are also provided as exports to spreadsheets,
  Word documents, RSS feed and ical (which can be used in Google calendar).
* There are tools for shifting lessons up/down in your course offering, when you
  find that the initial plan needs updates.
* Lesson dates in a course offering are exportable, which is useful if many
  teachers (or course offerings) have exactly the same schedule. Create once,
  then export-and-import to add them to another offering.

Find this project at http://drupal.org/node/1861936. Bug reports, feature
requests, ideas and general feedfback is welcome.
