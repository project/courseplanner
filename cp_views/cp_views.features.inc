<?php
/**
 * @file
 * cp_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cp_views_views_api() {
  return array("version" => "3.0");
}
